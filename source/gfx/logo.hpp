#ifndef __CAESARIA_GFX_LOGO_H_INCLUDED__
#define __CAESARIA_GFX_LOGO_H_INCLUDED__

#include "engine.hpp"

namespace splash
{

void initialize(const std::string& name);

} //end namespace splash

#endif //__CAESARIA_GFX_LOGO_H_INCLUDED__
